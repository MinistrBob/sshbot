﻿using log4net;
using System;
using System.Diagnostics;
using System.IO;
using System.Threading;
using System.Windows.Forms;

namespace SSHBot
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            log4net.Config.XmlConfigurator.Configure();
            AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(CurrentDomain_UnhandledException);
            Application.SetUnhandledExceptionMode(UnhandledExceptionMode.CatchException);
            Application.ThreadException += new System.Threading.ThreadExceptionEventHandler(Application_ThreadException);

            // Проверка есть ли запущеные копии программы, реализована с помощью мьютекса
            bool onlyInstance;
            // Создаем мьютекс
            using (Mutex mtx = new Mutex(true, "SSHBot.exe", out onlyInstance))
            {
                // Если он создается, значит ни одной копии еще не запущено - просто запускаем.
                if (onlyInstance)
                {
                    RunSSHBot();
                }
                // Если он НЕ создается, значит уже есть запущеные экземпляры - спрашиваем юзера что делать
                else
                {
                    if (DialogResult.Yes == MessageBox.Show(
                                           "Application \"SSHBot \" is already running, you want to start another copy?",
                                           "Message",
                                           MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation))
                    {
                        RunSSHBot();
                    }

                }
            }
        }

        private static void RunSSHBot()
        {
            try
            {
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                Application.Run(new Form1());
            }
            catch (Exception ex)
            {
                LogUnhandledException(ex);
            }
        }

        // ---------------------------------------------------------------------------------------------------------------
        static void Application_ThreadException(object sender, System.Threading.ThreadExceptionEventArgs e)
        {
            LogUnhandledException(e.Exception);
        }

        // ---------------------------------------------------------------------------------------------------------------

        static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            LogUnhandledException(e.ExceptionObject as Exception);
        }

        /// <summary>
        /// запись информации о необработанной ошибке в файл
        /// </summary>
        public static void LogUnhandledException(Exception exception)
        {
            string msg = string.Format(
                        @"Произошла непредвиденная ошибка и программа была закрыта.
                        Текст ошибки: {0}
                        Стек вызовов: {1}", exception.Message, exception.StackTrace);

            // Пишем файл с ошибкой и стеком в папку откуда запускается программа, если не получается - в папку %TEMP%
            try
            {
                File.WriteAllText(
                        Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "!!!SSHBot_exception.txt"),
                        DateTime.Now + "\r\n" + msg, System.Text.Encoding.GetEncoding(1251));
            }
            catch (Exception)
            {

                try
                {
                    File.WriteAllText(
                        Path.Combine(Path.GetTempPath(), "!!!SSHBot_exception.txt"),
                        DateTime.Now + "\r\n" + msg, System.Text.Encoding.GetEncoding(1251));
                }
                catch (Exception)
                {


                }
            }

            ILog log = LogManager.GetLogger(typeof(Program));
            log.Fatal(msg, exception);

            try
            {
                if (!EventLog.SourceExists("SSHBot.exe"))
                    EventLog.CreateEventSource("SSHBot.exe", "Application");
                EventLog el = new EventLog("Application");
                el.Source = "SSHBot.exe";
                el.WriteEntry(msg, EventLogEntryType.Error, 999, 0);
                el.Close();
            }
            catch
            {
            }
        }
    }
}
