﻿using log4net;
using Renci.SshNet;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Windows.Forms;

namespace SSHBot
{
    public partial class Form1 : Form
    {
        internal Settings settings;
        //public Settings Settings
        //{
        //    get { return settings; }
        //    set { settings = value; }
        //}
        // Для логирования
        private static readonly ILog Log = LogManager.GetLogger(typeof(Program));

        // Для переименования хоста нужно менять
        // /etc/sysconfig/network
        // и
        // hostname mynewhost

        // Папка в которой происходит работа
        private string baseLocalDir = @"d:\!SAVE\MKS";
        //private string baseLocalDir = @"c:\TEMP\SSHBot\MKS";
        private string serverLocalDir = "";
        private string currentResult = "";
        private string[] currentArray;
        private int counter1;

        public Form1()
        {
            InitializeComponent();
        }

        class ExceptionLogger
        {
            Exception _ex;
            public string errorMessage;

            public ExceptionLogger(Exception ex)
            {
                _ex = ex;
            }

            public void DoLog()
            {
                //Console.WriteLine(_ex.ToString()); //Will display en-US message
                //tbConsole.AppendText("ERROR: " + err.Message + "\n");
                errorMessage = _ex.Message.ToString();
            }
        }

        private void btStart_Click(object sender, EventArgs e)
        {
            ComputersLists servers = new ComputersLists();
            var currList = servers.serversRCOD;

            counter1 = 1;

            foreach (KeyValuePair<string, string> keyValue in currList)
            {
                // keyValue.Value представляет класс Person
                //Console.WriteLine(keyValue.Key + " - " + keyValue.Value.Name);

                tbConsole.AppendText("\n-----------------------------------------------  " + String.Format("Сервер {0} из {1}", counter1, currList.Count) + "  -----------------------------------------------" + Environment.NewLine);
                try
                {
                    tbConsole.AppendText("Work with server - " + keyValue.Key + "\n");
                    // Setup Credentials and Server Information
                    ConnectionInfo ConnNfo = new ConnectionInfo(keyValue.Key, 22, "root", new AuthenticationMethod[]
                    {
                        // Pasword based Authentication
                        new PasswordAuthenticationMethod("root","password"),
                        // Key Based Authentication (using keys in OpenSSH Format)
                        new PrivateKeyAuthenticationMethod("rsa.key"),
                    }
                    );

                    //var timeout = new TimeSpan(0,0,60);
                    //ConnNfo.Timeout = timeout;

                    // Создаю отдельную для каждого сервера папку для резервных копий файлов
                    tbConsole.AppendText(String.Format("Create local folder {0}\n", keyValue.Key));
                    serverLocalDir = Path.Combine(baseLocalDir, keyValue.Key);
                    Directory.CreateDirectory(serverLocalDir);

                    GetServersNames(keyValue, ConnNfo);

                    RenameServer(keyValue, ConnNfo);
                    DownloadFiles(keyValue, ConnNfo);
                    ChangeRosolvConf(keyValue, ConnNfo);
                    ConfigureDNS(keyValue, ConnNfo);
                    MainTest(keyValue, ConnNfo);

                }
                catch (Exception err)
                {
                    //MessageBox.Show(err.Message);
                    tbConsole.AppendText("ERROR: " + err.Message + "\n");
                    //tbConsole.AppendText("HResult: " + err.HResult + "\n");
                    tbConsole.AppendText("StackTrace: " + err.StackTrace + "\n");

                    ExceptionLogger el = new ExceptionLogger(err);
                    System.Threading.Thread t = new System.Threading.Thread(el.DoLog);
                    //t.CurrentUICulture = new System.Globalization.CultureInfo("en-US");
                    t.CurrentCulture = CultureInfo.InvariantCulture;
                    t.CurrentUICulture = CultureInfo.InvariantCulture;
                    t.Start();
                    tbConsole.AppendText("ERROR: " + el.errorMessage + "\n");
                }
                counter1++;
            }

            //pbFileStrim.Value = 0;
            //pbFileStrim.Maximum = 0;
            pbFileStrim.Invoke((MethodInvoker)delegate { pbFileStrim.Value = 0; });
            //tbConsole.AppendText(String.Format("pbFileStrim.Value = {0}\n", pbFileStrim.Value));
            //tbConsole.AppendText(String.Format("pbFileStrim.Minimum = {0}\n", pbFileStrim.Minimum));
            //tbConsole.AppendText(String.Format("pbFileStrim.Maximum = {0}\n", pbFileStrim.Maximum));
            tbConsole.AppendText("----------------------------------------------------------------------------------------------" + Environment.NewLine + "END OF WORK");

            //// Execute (SHELL) Commands
            //using (var sshclient = new SshClient(ConnNfo))
            //{
            //    sshclient.Connect();

            //    // quick way to use ist, but not best practice - SshCommand is not Disposed, ExitStatus not checked...
            //    Console.WriteLine(sshclient.CreateCommand("cd /tmp && ls -lah").Execute());
            //    Console.WriteLine(sshclient.CreateCommand("pwd").Execute());
            //    Console.WriteLine(sshclient.CreateCommand("cd /tmp/uploadtest && ls -lah").Execute());
            //    sshclient.Disconnect();
            //}
            //Console.ReadKey();


        }

        private void RenameServer(KeyValuePair<string, string> keyValue, ConnectionInfo ConnNfo)
        {
            // Переименование компьютера
            tbConsole.AppendText("START RENAME COMPUTER\n");
            using (var sshclient = new SshClient(ConnNfo))
            {
                tbConsole.AppendText("Connect to " + keyValue.Key + " ...\n");
                sshclient.Connect();
                //ExecuteCommand(sshclient, @"cd /etc/sysconfig/network-scripts; pwd; ls ifcfg-*");
                //ExecuteCommand(sshclient, @"pwd");
                ExecuteCommand(sshclient, @"ls /etc/sysconfig/network-scripts/ifcfg-*", "");
                currentArray = currentResult.Split(new string[] { "\r\n", "\n" }, StringSplitOptions.RemoveEmptyEntries);
                ExecuteCommand(sshclient, @"hostname " + keyValue.Value, "");
                ExecuteCommand(sshclient, String.Format(@"sed -i 's/^HOSTNAME=.*/HOSTNAME={0}/g' /etc/sysconfig/network", keyValue.Value), "");
                tbConsole.AppendText("Disconnect from " + keyValue.Key + " ...\n");
                sshclient.Disconnect();
            }
            tbConsole.AppendText("END RENAME COMPUTER\n");
        }

        private void DownloadFiles(KeyValuePair<string, string> keyValue, ConnectionInfo ConnNfo)
        {
            // Скачиваю набор файлов
            tbConsole.AppendText("START DOWNLOAD FILES\n");
            using (var sftp = new SftpClient(ConnNfo))
            {
                string targetFile;
                string sourceDir;

                tbConsole.AppendText("Connect to " + keyValue.Key + " ...\n");
                sftp.Connect();

                // Download current file
                foreach (var item in currentArray)
                {
                    if (!item.Contains("ifcfg-lo"))
                    {
                        targetFile = item;
                        sourceDir = serverLocalDir;
                        DownloadFile(sftp, targetFile, sourceDir);
                    }
                }

                tbConsole.AppendText("Disconnect from " + keyValue.Key + " ...\n");
                sftp.Disconnect();
            }
            tbConsole.AppendText("END DOWNLOAD FILES\n");
        }

        private void ChangeRosolvConf(KeyValuePair<string, string> keyValue, ConnectionInfo ConnNfo)
        {
            // Сохраняю и закачиваю resolv.conf
            tbConsole.AppendText("START CHANGE RESOLV.CONF\n");
            using (var sftp = new SftpClient(ConnNfo))
            {
                string targetFile;
                string sourceDir;

                sftp.Connect();
                tbConsole.AppendText("Connect...\n");

                // Save reserved copy file
                targetFile = @"/etc/resolv.conf";
                sourceDir = serverLocalDir;
                DownloadFile(sftp, targetFile, sourceDir);

                // Upload file
                targetFile = @"d:\!SAVE\MKS\resolv.conf";
                sourceDir = @"/etc";
                UploadFile(sftp, targetFile, sourceDir);

                sftp.Disconnect();
            }
            tbConsole.AppendText("END CHANGE RESOLV.CONF\n");
        }

        private void ConfigureDNS(KeyValuePair<string, string> keyValue, ConnectionInfo ConnNfo)
        {
            // Изменение настроек DNS
            tbConsole.AppendText("START CONFIGURE DNS\n");
            using (var sshclient = new SshClient(ConnNfo))
            {
                tbConsole.AppendText("Connect to " + keyValue.Key + " ...\n");
                sshclient.Connect();

                // Configure network settings
                // Просто удаляю настройки DNS из всех /etc/sysconfig/network-scripts/ifcfg-*, чтобы они брались из /etc/resolv.conf
                foreach (var item in currentArray)
                {
                    if (!item.Contains("ifcfg-lo"))
                    {
                        tbConsole.AppendText("Edit file " + item + "\n");
                        ExecuteCommand(sshclient, String.Format(@"sed -i '/^DNS.*/d' {0}", item), "");
                        //ExecuteCommand(sshclient, String.Format(@"echo ""DNS2=10.128.86.2"" >> {0}", item));
                        //ExecuteCommand(sshclient, String.Format(@"echo ""DNS3=8.8.8.8"" >> {0}", item));
                    }
                }
                tbConsole.AppendText("Disconnect from " + keyValue.Key + " ...\n");
                sshclient.Disconnect();
            }
            tbConsole.AppendText("END CONFIGURE DNS\n");
        }

        private void MainTest(KeyValuePair<string, string> keyValue, ConnectionInfo ConnNfo)
        {
            // Общий тест выполненых работ
            tbConsole.AppendText("START MAIN TEST\n");
            using (var sshclient = new SshClient(ConnNfo))
            {
                tbConsole.AppendText("Connect to " + keyValue.Key + " ...\n");
                sshclient.Connect();

                ExecuteCommand(sshclient, @"hostname", "");
                ExecuteCommand(sshclient, @"cat /etc/sysconfig/network", "");
                ExecuteCommand(sshclient, @"cat /etc/resolv.conf", "");
                ExecuteCommand(sshclient, @"cat /etc/sysconfig/network-scripts/ifcfg-*", "");
                ExecuteCommand(sshclient, @"nslookup rr.cod", "");
                ExecuteCommand(sshclient, @"nslookup cod-bal01.rr.cod", "");

                tbConsole.AppendText("Disconnect from " + keyValue.Key + " ...\n");
                sshclient.Disconnect();
            }
            tbConsole.AppendText("END MAIN TEST\n");
        }

        private void GetServersNames(KeyValuePair<string, string> keyValue, ConnectionInfo ConnNfo)
        {
            // Список имен серверов в сети
            tbConsole.AppendText("START GET SERVER NAMES\n");
            using (var sshclient = new SshClient(ConnNfo))
            {
                tbConsole.AppendText("Connect to " + keyValue.Key + " ...\n");
                sshclient.Connect();
                ExecuteCommand(sshclient, @"hostname", keyValue.Key);
                tbConsole.AppendText("Disconnect from " + keyValue.Key + " ...\n");
                sshclient.Disconnect();
            }
            tbConsole.AppendText("END GET SERVER NAMES\n");
        }

        // Execute Any Command
        // если передана strLog - сцепляем с результатом без вывода самой команды
        private void ExecuteCommand(SshClient sshclient, string command, string strLog)
        {
            using (var cmd = sshclient.CreateCommand(command))
            {
                if (string.IsNullOrEmpty(strLog))
                {
                    tbConsole.AppendText("# " + cmd.CommandText + "\n"); 
                }
                cmd.Execute();
                if (cmd.ExitStatus == 0)
                {
                    // Заменяем переносы строки \n на \r\n чтобы в логе переносились строки (просто \n не переноситься)
                    currentResult = cmd.Result.Replace("\n", "\r\n");
                    if (string.IsNullOrEmpty(strLog))
                    {
                        tbConsole.AppendText(currentResult + "\n");
                    }
                    else
                    {
                        tbConsole.AppendText(strLog + " - " + currentResult + "\n");
                    }   
                }
                else
                {
                    currentResult = "";
                    tbConsole.AppendText("ERROR:" + "\n");
                    tbConsole.AppendText(cmd.Error + "\n");
                }
                //tbConsole.AppendText(String.Format("Return Value = {0}\n", cmd.ExitStatus));
            }
        }

        // Upload A File
        private void UploadFile(SftpClient sftp, string targetFile, string sourceDir)
        {
            pbFileStrim.Value = 0;
            string fileName = Path.GetFileName(targetFile);
            string sourceFile = sourceDir + "/" + fileName;
            tbConsole.AppendText("ChangeDirectory to " + sourceDir + "\n");
            sftp.ChangeDirectory(sourceDir);
            tbConsole.AppendText("Upload file " + targetFile + " to " + sourceFile + "\n");
            using (var uplfileStream = System.IO.File.OpenRead(targetFile))
            {
                // Set progress bar maximum on foreground thread
                pbFileStrim.Invoke((MethodInvoker)delegate { pbFileStrim.Maximum = (int)uplfileStream.Length; });
                sftp.UploadFile(uplfileStream, sourceFile, true, UpdateProgresBar);
            }
            tbConsole.AppendText("Upload complite\n");
            pbFileStrim.Value = 0;
        }

        // Upload A File
        private void DownloadFile(SftpClient sftp, string targetFile, string sourceDir)
        {
            pbFileStrim.Value = 0;
            string fileName = Path.GetFileName(targetFile);
            string sourceFile = Path.Combine(sourceDir, fileName);
            //tbConsole.AppendText("ChangeDirectory to " + sourceDir + "\n");
            //sftp.ChangeDirectory(sourceDir);
            tbConsole.AppendText("Download file " + targetFile + " to " + sourceFile + "\n");
            var file = sftp.Get(targetFile);
            using (var uplfileStream = System.IO.File.Open(sourceFile, FileMode.Create, FileAccess.ReadWrite))
            {
                // Set progress bar maximum on foreground thread
                pbFileStrim.Invoke((MethodInvoker)delegate { pbFileStrim.Maximum = (int)file.Length; });
                sftp.DownloadFile(targetFile, uplfileStream, UpdateProgresBar);
            }
            tbConsole.AppendText("Download complite\n");
            pbFileStrim.Value = 0;
        }

        private void UpdateProgresBar(ulong uploaded)
        {
            // Update progress bar on foreground thread
            pbFileStrim.Invoke((MethodInvoker)delegate { pbFileStrim.Value = (int)uploaded; });
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            settings = new Settings();
            settings = Settings.LoadFromFile(settings.SettingsFileName);
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            settings.SaveToFile(settings.SettingsFileName);
        }
    }
}
