﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SSHBot
{
    [Serializable]
    class Server
    {
        public string Name;
        public string IP;
        public string UserName;
        public string Password;
        public string Port;
        public CommandList commandList;
    }
}
