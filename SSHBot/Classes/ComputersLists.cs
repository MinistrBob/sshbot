﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SSHBot
{
    class ComputersLists
    {
        // Тестовый сервер
        public Dictionary<string, string> serversTEST = new Dictionary<string, string>
        {
            //["172.26.12.2"] = "server.rr.cod"
            //["10.128.82.2"] = "cod-bal01.rr.cod"
            //["10.128.82.3"] = "cod-bal02.rr.cod"
            //["10.128.82.4"] = "rcod-zab01.rr.rcod",
            //["10.128.82.5"] = "rcod-zab02.rr.rcod"
            //["10.128.86.3"] = "rcod-bal02.rr.rcod"
        };

        // Сервера ЦОД
        public Dictionary<string, string> serversCOD = new Dictionary<string, string>
        {
            //["10.128.82.2"] = "cod-bal01.rr.cod",
            //["10.128.82.3"] = "cod-bal02.rr.cod",
            ["10.128.82.4"] = "cod-zab01.rr.cod",
            ["10.128.82.5"] = "cod-zab02.rr.cod",
            ["10.128.81.1"] = "cod-wild01.rr.cod",
            ["10.128.81.2"] = "cod-wild02.rr.cod",
            ["10.128.81.3"] = "cod-wild03.rr.cod",
            ["10.128.81.4"] = "cod-wild04.rr.cod",
            //["10.128.81.5"] = "cod-post01.rr.cod",
            ["10.128.81.6"] = "cod-post02.rr.cod",
            ["10.128.81.7"] = "cod-post03.rr.cod",
            //["10.128.81.8"] = "cod-post04.rr.cod",
            ["10.128.81.9"] = "S-RR-KSND-01",
            ["10.128.81.10"] = "S-RR-KSND-02",
            ["10.128.81.11"] = "S-RR-KSND-03",
            ["10.128.81.12"] = "S-RR-KSND-04",
            ["10.128.81.13"] = "S-RR-KSND-05",
            ["10.128.81.14"] = "S-RR-KSND-06",
            ["10.128.81.15"] = "S-RR-KSND-07",
            ["10.128.81.16"] = "S-RR-KSND-08",
            ["10.128.81.17"] = "S-RR-KSND-09",
            ["10.128.81.18"] = "S-RR-KSND-10",
            ["10.128.81.19"] = "S-RR-KSND-11",
            ["10.128.81.20"] = "S-RR-KSND-12",
            ["10.128.81.21"] = "S-RR-KSND-13",
            ["10.128.81.22"] = "S-RR-KSND-14",
            ["10.128.81.23"] = "S-RR-KSND-15",
            ["10.128.81.24"] = "S-RR-KSND-16",
            ["10.128.81.25"] = "S-RR-KSND-17",
            ["10.128.81.26"] = "S-RR-KSND-18",
            ["10.128.81.27"] = "S-RR-KSND-19",
            ["10.128.81.28"] = "S-RR-KSND-20",
            ["10.128.81.29"] = "S-RR-KSND-21",
            ["10.128.81.30"] = "S-RR-KSND-22"
        };

        // Сервера РЦОД
        public Dictionary<string, string> serversRCOD = new Dictionary<string, string>
        {
            //["10.128.86.3"] = "rcod-bal02.rr.rcod",
            //["10.128.82.4"] = "rcod-zab01.rr.rcod",
            //["10.128.82.5"] = "rcod-zab02.rr.rcod",
            ["10.128.85.1"] = "rcod-wild01.rr.rcod",
            ["10.128.85.2"] = "rcod-wild02.rr.rcod",
            ["10.128.85.3"] = "rcod-wild03.rr.rcod",
            ["10.128.85.4"] = "rcod-wild04.rr.rcod",
            //["10.128.85.5"] = "rcod-post01.rr.rcod",
            ["10.128.85.6"] = "rcod-post02.rr.rcod",
            ["10.128.85.7"] = "rcod-post03.rr.rcod",
            //["10.128.85.8"] = "rcod-post04.rr.rcod",
            ["10.128.85.9"] = "S-RR-KSND-23",
            ["10.128.85.10"] = "S-RR-KSND-24",
            ["10.128.85.11"] = "S-RR-KSND-25",
            ["10.128.85.12"] = "S-RR-KSND-26",
            ["10.128.85.13"] = "S-RR-KSND-27",
            ["10.128.85.14"] = "S-RR-KSND-28",
            ["10.128.85.15"] = "S-RR-KSND-29",
            ["10.128.85.16"] = "S-RR-KSND-30",
            ["10.128.85.17"] = "S-RR-KSND-31",
            ["10.128.85.18"] = "S-RR-KSND-32",
            ["10.128.85.19"] = "S-RR-KSND-33",
            ["10.128.85.20"] = "S-RR-KSND-34",
            ["10.128.85.21"] = "S-RR-KSND-35",
            ["10.128.85.22"] = "S-RR-KSND-36",
            ["10.128.85.23"] = "S-RR-KSND-37",
            ["10.128.85.24"] = "S-RR-KSND-38",
            ["10.128.85.25"] = "S-RR-KSND-39",
            ["10.128.85.26"] = "S-RR-KSND-40",
            ["10.128.85.27"] = "S-RR-KSND-41",
            ["10.128.85.28"] = "S-RR-KSND-42",
            ["10.128.85.29"] = "S-RR-KSND-43",
            ["10.128.85.30"] = "S-RR-KSND-44"
        };
    }
}
