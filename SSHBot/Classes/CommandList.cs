﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SSHBot
{
    [Serializable]
    class CommandList
    {
        public string Name;
        public string Description;
        public List<Command> commandList;
    }
}
