﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SSHBot.Classes
{
    class Project
    {
        public string Name; // Имя проекта
        public string Description; // Описание проекта
        public string Log; // Во время работы сюда может писаться лог, а потом его можно выводить куда-то
        public string ProjectFolder; // Папка в которой будут собраны все файлы для проекта и лог
        public CommandList commandList; // 
        public ServerList serverList;
    }
}
