﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml.Serialization;

namespace SSHBot
{
    /*
    Где можно хранить настройки программы:
	1) Program Files (папка самой программы) - Здесь для пользователя может не хватить прав и настройки одни на всех.
	2) My Documents - Эта папка для документов, а не для хранения настроек программ. Единственное исключение можно сделать если эта папка синхронизируется, например с облаком или другим компьютером.
	   XP: Documents and Settings\User\My Documents
       Vista: Users\User\Documents
    3) User\Application Data - Файлы из этой папки входят в состав перемещаемых профилей (roaming profiles, per-user). Т.е. если вы хотите, чтобы данные Вашей программы могли бы перемещаться вместе с профилем пользователя храните их в этой папке.
	   XP: Documents and Settings\User\Application Data
       Vista: Users\User\AppData\Roaming
    4) User\Local Settings\Application Data - В этой папке можно хранить файлы, не входящие в состав перемещаемых профилей (per-user-per-machine), а так же временные и большие по размеру файлы, перемещение которых либо не требуется (временные, кэш, прочий рабочий «мусор»), либо потребует значительных временных затрат (храните файл размером в пару сотен мегабайт в перемещаемом профиле и пользователь обязательно скажет Вам «спасибо»).
	   XP: Documents and Settings\User\Local Settings
       Vista: Users\User\AppData\Local
    5) All Users\Application data - В этой папке можно хранить файлы, которые являются общими для всех пользователей компьютера, например общая база данных, набор общих документов, клипарт и т.д. Эта папка не входит в состав перемещаемых профилей (per-machine). Обычные пользователи (не администраторы) имеют к этой папке доступ в режиме только для чтения.
       XP: Documents and Settings\All Users\Application Data
       Vista: ProgramData
    Выбираю по умолчанию вариант 3.     
    */

    [Serializable]
    public class Settings
    {
        //private enum SettingsFileNameList { SettingsFileName1 = 1, SettingsFileName2 = 2, SettingsFileName3 = 3, SettingsFileName4 = 4, SettingsFileName5 = 5 };
        //private List<string> listOfPaths;

        [XmlIgnore]
        public string SettingsFileName { get; set; }  // Файл конфига

        public int PosX { get; set; }               // Позиция основного окна X
        public int PosY { get; set; }               // Позиция основного окна Y
        public int Width { get; set; }              // Ширина основного окна
        public string DBPath { get; set; }          // Путь к базе данных

        // For test
        public int HistoryMaxLength { get; set; }

        /*
        public SerializableFont Font { get; set; }  // Шрифт основного окна
        public bool FontSpecified { get; set; }
        public bool StayOnTop { get; set; }         // Поверх других окно
        public int Opacity { get; set; }            // Прозрачность
        public bool IncreaseWidthWindow { get; set; } // Увеличивать ширину окна по мере ввода?

        public Point FormHelpLocation { get; set; } // Позиция окна Help

        [XmlIgnore]
        public Color BackColor { get; set; }

        [XmlIgnore]
        public Color ForeColor { get; set; }

        public int HistoryMaxLength { get; set; }

        public string HistoryExportDivider { get; set; } // разделитель полей для экспорта в текстовый файл
        */

        public Settings()
        {
            List<string> listOfPaths = new List<string>();
            string[] orderPaths = Properties.Settings.Default.TabOrderOfFoldersWhichCanBeStoredForTheSettingsFile.Split(',');
            // Если есть особый путь хранения настроек - сначала будем искать там
            if (!string.IsNullOrEmpty(Properties.Settings.Default.QuiteSpecialPath))
            {
                listOfPaths.Add(Properties.Settings.Default.QuiteSpecialPath);
            }
            // Потом все остальные пути в особом порядке, заданном в настройках
            foreach (var item in orderPaths)
            {
                switch (item)
                {
                    case "1":
                        listOfPaths.Add(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "SSHBot.Settings.xml"));
                        break;
                    case "2":
                        listOfPaths.Add(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), @"SSHBot\SSHBot.Settings.xml"));
                        break;
                    case "3":
                        listOfPaths.Add(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), @"SSHBot\SSHBot.Settings.xml"));
                        break;
                    case "4":
                        listOfPaths.Add(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), @"SSHBot\SSHBot.Settings.xml"));
                        break;
                    case "5":
                        listOfPaths.Add(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData), @"SSHBot\SSHBot.Settings.xml"));
                        break;
                }

            }
            // Ищем файл настроек по всем путям
            foreach (var item in listOfPaths)
            {
                if (File.Exists(item))
                {
                    SettingsFileName = item;
                }
            }
            // Если файл настроек нигде не найден - задаём путь для его хранения
            if (string.IsNullOrEmpty(SettingsFileName))
            {
                SettingsFileName = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), @"SSHBot\SSHBot.Settings.xml");
            }

            HistoryMaxLength = 100;

            /*
            BackColor = SystemColors.Window;
            ForeColor = SystemColors.WindowText;
            Opacity = 100;
            IncreaseWidthWindow = true;
            HistoryExportDivider = ";";
            */
        }

        /*
        public int BackColorRgb
        {
            get { return BackColor.ToArgb(); }
            set { BackColor = Color.FromArgb(value); }
        }

        public int ForeColorRgb
        {
            get { return ForeColor.ToArgb(); }
            set { ForeColor = Color.FromArgb(value); }
        }
        */

        public virtual void SaveToFile(string FileName)
        {
            using (StreamWriter sw = new StreamWriter(FileName, false, Encoding.GetEncoding(1251)))
            {
                XmlSerializer xs = new XmlSerializer(GetType());
                xs.Serialize(sw, this);
            }
        }

        public static Settings LoadFromFile(string FileName)
        {
            try
            {
                if (!Directory.Exists(Path.GetDirectoryName(FileName)))
                    Directory.CreateDirectory(Path.GetDirectoryName(FileName));
                using (StreamReader sr = new StreamReader(FileName, Encoding.GetEncoding(1251)))
                {
                    XmlSerializer xs = new XmlSerializer(typeof(Settings));
                    return xs.Deserialize(sr) as Settings;
                }
            }
            catch
            {
                return new Settings();
            }
        }

        /*
        public Config ShallowCopy()
        {
            return (Config)this.MemberwiseClone();
        }
         * */
    }
}
